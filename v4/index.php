<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="index.css"/>
    <title>Document</title>
</head>
<body>


<div id="phone">
    <div id="phone-content">
        <div id="place-message">TU Eindhoven</div>
        <div id="phone-rotate-message">
            <div id="rotate-message-top">
                <div id="rotate-message-icon">
                    <img src="rotate.png" size="200"/>
                </div>
                <div id="rotate-message-text">
                    Please rotate your phone to enter VR
                </div>
            </div>
            <div id="rotate-message-bottom">
                <a href="#" id="removeBanner"><b>GOT IT!</b></a>
            </div>
        </div>
        <div class="item">
            Info about spot here please!
        </div>
    </div>
</div>

<div id="VR">
    <iframe src="https://sandervanderburgt.com/storytelling/v3"></iframe>
</div>

<!--<div id="alpha">Hello</div>-->
<!--<div id="rotation">Not rotated</div>-->

<script type="text/javascript" src="index.js"></script>

</body>
</html>