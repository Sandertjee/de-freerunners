<html>
<head>
	<script src="https://aframe.io/releases/0.9.0/aframe.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/main.js"> </script>
	<link rel="stylesheet" type="text/css" href="styles.css" />
    <meta name="apple-mobile-web-app-capable" content="yes">
</head>

<body>
<!--	<div id="youtube" style="height: 100%; width: 100%;"></div>-->
	<a-scene freerun>

		<a-entity id="scene1" visible="true">
<!--			<a-entity id="displayText" text="value:Kies een van de 3 mogelijkheden om bij punt B te komen; alphaTest: 0.67; width: 1.62; wrapCount: 44.71" position="0.02751 2.15 -0.86126" rotation="0 0 -0.02"></a-entity>-->


            <a-image src="img/info-icon.png" width="0.3"height="0.3" position="2.763 1.541 -2.300" rotation="0 -50 0" material="" geometry="" scale="1 1.3 1"> </a-image>
            <a-entity id="displayText" text="value:Plein eindhoven; color: black; alphaTest: 0.67; width: 1.62; wrapCount: 44.71" position="3.799 1.161 -1.561" rotation="0 -50 0" scale="2 2 2"></a-entity>


            <a-image firstvideo src="img/arrow-up.png" width="0.3" rotation="" height="0.3" position="0.012 1.324 -3.890" material="" geometry="" scale="1 1.3 1"> </a-image>

            <a-entity id="displayText" text="value:Video!; color: black; alphaTest: 0.67; width: 1.62; wrapCount: 44.71" position="-3.686 2.773 -2.977" rotation="0 50 0" scale="2 2 2"></a-entity>
            <a-video videonormal src="video's/freerunintro.mp4" id="videonormalid" autoplay="false" playsinline="true" width="3" height="1.5" rotation="0 50 0" position="-4.103 1.896 -3.096"></a-video>

            <a-entity id="displayText" text="value:Kijk mee met bart!; color: black; alphaTest: 0.67; width: 1.62; wrapCount: 44.71" position="-3.678 0.896 -2.986" rotation="0 50 0" scale="2 2 2"></a-entity>
            <a-video videofp src="video's/puntB.mp4" id="videofpid" playsinline="true" autoplay="false" width="3" height="1.5" rotation="0 50 0" position="-4.112 -0.046 -3.108"></a-video>


<!--			<a-image secondvideo src="img/freeruniconB.png" width="0.3" rotation="0 29.999999999999996" height="0.3" position="-0.99816 1.262 -1.78105" material="" geometry="" scale="0.65 0.9 0.65"> </a-image>-->
<!--			<a-image thirdvideo src="img/freeruniconC.png" width="0.3" rotation="0 -10" height="0.3" position="0.46053 1.277 -1.66459" material="" geometry="" scale="0.55 0.8 0.55"> </a-image>-->


			<a-sky sky id="image-360" radius="10" src="img/puntA.jpg" material="" geometry="" position="0.97 0.07" rotation="0 -81.24"></a-sky>
		</a-entity>

		<a-entity id="scene2" visible="false">
<!--			<a-entity id="displayText" text="value: Je hebt punt B bereikt!; color: black; alphaTest: 0.67; width: 1.62; wrapCount: 44.71" position="0.16045 1.615 -0.73339" rotation="0 29.999999999999996 0.05"></a-entity>-->
			<a-sky sky="" id="image-360deg" radius="10" src="img/puntB.jpg" material="" geometry="" position="0.04 -0.72" rotation="0 100"></a-sky>
		</a-entity>

	
		<a-camera><a-cursor id="cursor"></a-cursor></a-camera>

	</a-scene>
</body>

<script>

	var cursor = document.getElementById('cursor');
	var textbox = document.getElementById('displayText');

    function load(page){
        $("#youtube").load(page);
    }
 
    // $("a-scene").hide();
    // load("youtube.php?videoId=4RvAak4RKrA&unload=scene2&load=scene1");
 
</script>

</html>