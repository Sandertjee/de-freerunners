<?php
$videoid = $_GET["videoId"];
$load = $_GET["load"];
$unload = $_GET["unload"];
 
?>
 
 
<div id="player"></div>
 
 
<script src="https://www.youtube.com/player_api"></script>
 
 
<script>
    var videoId = "<?php echo $videoid; ?>";
    var loadscene = "<?php echo $load; ?>";
    var unloadscene = "<?php echo $unload; ?>";
 
    var fired_once;
 
    // create youtube player
    var player;
    function onYouTubePlayerAPIReady() {
        player = new YT.Player('player', {
            width: '1920',
            height: '1080',
            videoId: videoId,
            events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
            }
        });
 
        fired_once = true;
    }
 
    if(fired_once){
        onYouTubePlayerAPIReady();
    }
 
    // autoplay video
    function onPlayerReady(event) {
        event.target.playVideo();
    }
 
    // when video ends
    function onPlayerStateChange(event) {
        if(event.data === 0) {
            player.stopVideo();
            player = "";
            console.log("Loading vrview.php");
            document.getElementById(loadscene).setAttribute('visible', 'true');
            document.getElementById(unloadscene).setAttribute('visible', 'false');
            document.getElementById("youtube").innerHTML = "";
            $("a-scene").show();
            // load("vrview.php");
        }
    }
</script>