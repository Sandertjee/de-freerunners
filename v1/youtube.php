<div id="player"></div>


<script src="https://www.youtube.com/player_api"></script>


<script>

    var fired_once;


    // create youtube player
    var player;
    function onYouTubePlayerAPIReady() {
        player = new YT.Player('player', {
            width: '1920',
            height: '1080',
            videoId: '0Bmhjf0rKe8',
            events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
            }
        });

        fired_once = true;
    }


    if(fired_once){
        onYouTubePlayerAPIReady();
    }

    // autoplay video
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    // when video ends
    function onPlayerStateChange(event) {
        if(event.data === 0) {
            player.stopVideo();
            player = "";
            console.log("Loading vrview.php");
            load("vrview.php");
        }
    }
</script>