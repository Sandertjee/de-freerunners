<style>





    .pnlm-load-box{

    }

    /*.pnlm-lbar{*/
    /*color: indigo;*/
    /*border: #fff 1px solid;*/
    /*}*/

    /*.pnlm-lbar-fill{*/
    /*background-color: indigo;*/
    /*}*/

    /*.pnlm-load-button{*/


    /*}*/

    .pnlm-container{
        background-image: none;
    }

    #panorama {
        width: 100%;
        height: 930px;
    }
</style>

<div id="panorama">
    <script>W
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": "assets/own.jpeg",
            "autoLoad": true,
            "hotSpotDebug": true,
            /*
             * Uncomment the next line to print the coordinates of mouse clicks
             * to the browser's developer console, which makes it much easier
             * to figure out where to place hot spots. Always remove it when
             * finished, though.
             */
            //"hotSpotDebug": true,
            "hotSpots": [
                {
                    "pitch": 14.1,
                    "yaw": 1.5,
                    "type": "info",
                    "text": "Baltimore Museum of Art",
                    "URL": "https://artbma.org/"
                },
                {
                    "pitch": -9.4,
                    "yaw": 222.6,
                    "type": "info",
                    "text": "Art Museum Drive",
                    "clickHandlerFunc": loadnext,
                    "clickHandlerArgs": "youtube.php"
                },
                {
                    "pitch": -0.9,
                    "yaw": 144.4,
                    "type": "info",
                    "text": "North Charles Street"
                }
            ]
        });

        function loadnext(object, argument){
            console.log("Loading " + argument);
            load(argument);
        }
    </script>
</div>