<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="index.css"/>
    <title>Document</title>
</head>
<body>


<div id="phone">
    <div class="container">
        <div id="phone-content">
            <div id="place-message">TU Eindhoven</div>
            <div class="item">Info about spot here please!</div>
            <div id="bottom-nav">
<!--                <div id="bottom-left"><i class="fas fa-chevron-left"></i> &nbsp; Previous</div>-->
                <div id="bottom-left"></div>
                <div id="bottom-middle"><img id="enableVR" src="cardboard.svg"></div>
                <div id="bottom-right"></div>
<!--                <div id="bottom-right">Next &nbsp; <i class="fas fa-chevron-right"></i></div>-->
            </div>
        </div>
    </div>
</div>

<div id="VR">
    <iframe src="https://sandervanderburgt.com/storytelling/v3"></iframe>
</div>

<!--<div id="alpha">Hello</div>-->
<div id="rotation">Not rotated</div>


<script type="text/javascript" src="index.js"></script>

</body>
</html>