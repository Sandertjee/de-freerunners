// var absoluteElement = document.getElementById("absolute");
var alphaElement = document.getElementById("alpha");
var rotationElement = document.getElementById("rotation");
var phoneContent = document.getElementById("phone");
var vrElement = document.getElementById("VR");
var vrButton = document.getElementById("enableVR");

var buttonTriggered = false;
var wentSideways = false;


// var betaElement = document.getElementById("beta");
// var gammaElement = document.getElementById("gamma");


// alphaElement.innerHTML = "Hi there";


function handleOrientation(event) {
    var absolute = event.absolute;
    var alpha    = event.alpha;
    var beta     = event.beta;
    var gamma    = event.gamma;

    console.log(absolute);
    console.log(alpha);
    console.log(beta);
    console.log(gamma);

    // alphaElement.innerHTML = gamma;
    if(buttonTriggered){
        if(gamma > 70 || gamma < -70){
            // rotationElement.innerHTML = "Rotated!";
            // phoneContent.style.display = "none";
            // vrElement.style.display = "block";
            wentSideways = true;
        }
    }

    if(gamma < 70 && gamma > -70 && buttonTriggered && wentSideways){
        // rotationElement.innerHTML = "Not rotated!";
        phoneContent.style.display = "block";
        vrElement.style.display = "none";
        buttonTriggered = false;
        wentSideways = false;
    }


    // Do stuff with the new orientation data
}


window.addEventListener("deviceorientation", handleOrientation, true);


vrButton.addEventListener("click", enableVR);

function enableVR(){
    buttonTriggered = true;
    phoneContent.style.display = "none";
    vrElement.style.display = "block";
}



