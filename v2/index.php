<html>
<head>
    <script src="https://aframe.io/releases/0.9.0/aframe.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="triggers.js"></script>




</head>
<body>
<div id="youtube" style="height: 100%; width: 100%;"></div>

<a-scene freerun>

    <a-entity id="scene1" visible="true">
        <a-entity id="text" value="something" position="10 0 -4" rotation="0 -70 0"></a-entity>
        <a-plane boxonescene1 position="10 0 -4" rotation="0 -70 0" width="2" height="2" color="#2c82c9"></a-plane>
        <a-sky sky src="assets/own.jpeg"></a-sky>
    </a-entity>

    <a-entity id="scene2" visible="false">
        <a-plane boxonescene2 position="50 0 -4" rotation="0 -70 0" width="2" height="2" color="#2c82c9"></a-plane>
        <a-sky sky src="assets/workshop.png"></a-sky>
    </a-entity>

    <a-camera>
        <a-cursor id="cursor"></a-cursor>
    </a-camera>



</a-scene>
</body>

<script>

    var cursorVar = document.getElementById('cursor');

    function load(page){
        $("#youtube").load(page);
    }

    $("a-scene").hide();
    load("youtube.php?videoId=Wch3gJG2GJ4&unload=scene2&load=scene1");

</script>

<script>
    // console.log("Loading main");
    // load("vrview.php");

</script>

</html>