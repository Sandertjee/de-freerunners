AFRAME.registerComponent('boxonescene1', {
    init: function () {
        var el = this.el;  // <a-box>
        // Add event listeners here.

        el.addEventListener('click', function () {
            // el.setAttribute('scale', {x: 2, y: 1, z: 2});
            $("a-scene").hide();
            load("youtube.php?videoId=Wch3gJG2GJ4&unload=scene1&load=scene2");
        });

        el.addEventListener('mouseenter', function () {
            el.setAttribute('color', "#2574a9");
            cursorVar.setAttribute("color", "red");
        });

        el.addEventListener('mouseleave', function () {
            el.setAttribute('color', "#2c82c9");
            cursorVar.setAttribute("color", "black");
        });
    }
});




AFRAME.registerComponent('boxonescene2', {
    init: function () {
        var el2 = this.el;  // <a-box>
        // Add event listeners here.

        el2.addEventListener('click', function () {
            // el.setAttribute('scale', {x: 2, y: 1, z: 2});
            $("a-scene").hide();
            load("youtube.php?videoId=Wch3gJG2GJ4&unload=scene2&load=scene1");

        });

        el2.addEventListener('mouseenter', function () {
            el2.setAttribute('color', "#2574a9");
        });

        el2.addEventListener('mouseleave', function () {
            el2.setAttribute('color', "#2c82c9");
        });
    }
});